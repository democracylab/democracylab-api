
# Worklog
### Accidental [Nerd-snipe](https://xkcd.com/356/) :D 
9/30 TL;DnR: Many thanks to for comments/code/help! This made me realize I hadn't optimized nearly as well as I should have, in interest of time I kept returning to my [node script](https://gitlab.com/democracylab/democracylab-api/blob/master/db_setup/00_preprocess/Node%20(Use%20This%20For%20Now)/preprocess.js) and eventually got it to parse all 68M rows in about 40 min, which took care of the weekend sprint and got things online for the students to use today. That said, it seems that 1. I really probably should be using more Python, 2. I definitely want to learn Rust. I'm sitting on a lot more data to be parsed, so I'll be looking at both in the next few days, the fight isn't over yet. 


## Our story to date...
I am taking the output from a cleaning team and converting it to a [REST-ish API](https://docs.democracylab.io/docs/api/) which can be used more widely in a bunch of different ways, without requiring everyone to download and parse 20GB text file. I ultimately want this to live in a Postgres database, but I want it normalized and indexable so the queries aren't impossible to run.


### Attempt 1: Do the work in SQL
I first tried to import the TSV exactly as is, then assign a unique ID to each row, then slice it up using INSERT/SELECT statements, then apply proper types and indexes. On a test dataset this isn't screaming but works ok and we only need to do it once. On the full dataset, this ran for 40 hours and then crashed (womp). I think I could probably optimize, but it barely seems worth the effort, there's probably a better way.


### Attempt 2: Preprocess with Node
The Postgres import (COPY) is quite fast, so if I can pre-slice the data, we're good. I write a lot of JS, so node tends to be my swiss-army-knife (yeah yeah)... [I wrote this naive script](https://gitlab.com/democracylab/democracylab-api/blob/master/db_setup/00_preprocess/DEPRECATED/Node/preprocess.js) that slices and dices into a set of TSVs, which can be COPY imported into Postgres fairly quickly, then we can apply the referential integrity and indexes to them. This is running, but I project a completion time of 18-20 hours. More than twice as fast as the SQL, but not so hot.


### Attempt 3: [Yell for help on twitter](https://twitter.com/tezcatlipoca/status/1177968848504578050) and awesome friends send some alternative ideas...

  - Rust
    Using a compiled/typed language for this seems the most sensible, Rust is also completely new (to me), but [@urschrei provided some sample code](https://github.com/urschrei/hansort) to get me started. I am psyched about this and I think in the end it's going to be great, but I'm currently running full into my own lack of knowledge. I modified the code to get the slicing working... it runs, kind of... but we're still trying to load all the data into RAM at the top (I think), and the serializer seems to be taking an extremely long time to load. I'm probably doing it wrong. Rust probably has a streams lib or something better/similar? Meanwhile...
    
  - Python
    [@yerfington](https://gitlab.com/democracylab/democracylab-api/tree/master/db_setup/00_preprocess/Python/justinpy) sent me a python analysis script. This totally beats the pants off node, easily handling 68M rows in a few seconds, but it doesn't do the slicing. I probably should be using Python more, I could make this work. Meanwhile...

  - The Node script from Attempt 2 (which I left running) finally crashes after 15 hours, running out of heap space...
  
  
### Attempt 4: It occurs to me this is a lot of data

 - Specifically, it occurs to me that in terms of shear bulk, most of the data in this file lives in the plain-text of the debates and this weird tag-like column called "Ententies (sic)". I don't need to parse this content at all, I just want to shunt it to the side and give it an ID. I have 32GB of RAM on this machine, the datafile is 20GB... I bet I'm hitting swap issues...
  
 - I deputize my original node script to do only this task. It's not fast, but it handles splitting the data into two files in about 25 minutes. I now have two data files, the main one is still 68M rows, but about 10GB lighter.  
 
 - With this much lighter set of data, I try Rust again... and it still takes way too long to load. I'm sure I'm doing this wrong, but I think I need to find a filestream or rust equivalent so we can avoid loading the entire 68M into memory in one go (although it might be worth it, I assume once it's loaded we'll blast through everything in no time).


### Attempt 5: In which your hero remembers basic CS
- Hey... `for` loops are a lot faster than this fancy front-end declarative shit (I had a .find in there)... and yeah... arrays are fast, but hashes are way faster.

- LOL roughly 38x speed improvement.

- My [revised Node script](https://gitlab.com/democracylab/democracylab-api/blob/master/db_setup/00_preprocess/Node%20(Use%20This%20For%20Now)/preprocess.js) runs across all 68M lines in 36 min, which does the trick for now (but just think what we can do with some compiled code muhuhaha), but it keeps crashing at 38M rows. Everything appears ok, `htop` and activity monitor show that the script is using cycles and RAM but it doesn't seem out of range, it just... gets all bogged down somehow, and I don't know why. I suspect [GC](https://blog.risingstack.com/node-js-at-scale-node-js-garbage-collection/) or maybe [backpressure](https://nodejs.org/es/docs/guides/backpressuring-in-streams/) issue?

- I do some more optimization, flail a bit with [`drain` and `highwatermark`](https://stackoverflow.com/questions/18932488/how-to-use-drain-event-of-stream-writable-in-node-js) and also note that iTerm seems to be consuming an absurd amount of RAM. I reboot, pass node the "use all the RAM" flag `--max-old-space-size=29000` and... we do the thing, in 41 minutes. 

## But we can do better!
To be continued...
