--rsync -avzr ./TMP root@database.democracylab.io:/mnt/import

DROP TABLE IF EXISTS private.debate;
CREATE TABLE private.debate
(
    id                          serial NOT NULL, PRIMARY KEY (id),
    debate_sentence             text,
    debate_file                 text,
    debate_section              text,
    debate_section_sentence_id  text,
    debate_section_monologue_id text,
    debate_speech               text,
    debate_id                   text,
    debate_date                 text,
    debate_name                 text,
    debate_category             text,
    debate_text                 text,
    debate_speaker              text,
    debate_constituency         text,
    debate_house                text,
    debate_image                text,
    debate_column               text,
    debate_errata               text,
    debate_entity               text,
    debate_label                text,
    debate_junk                 text
);
COPY private.debate FROM PROGRAM 'sed ''s/\\/\\\\/g'' </home/andrew/hansard/data/debate.tsv';

DROP TABLE IF EXISTS private.debate_text;
CREATE TABLE private.debate_text
(
    id       serial NOT NULL,
    PRIMARY KEY (id),
    fulltext text,
    UNIQUE (id)
);
COPY private.debate_text FROM PROGRAM 'sed ''s/\\/\\\\/g'' </home/andrew/hansard/data/debateText.tsv';

DROP TABLE IF EXISTS private.debate_category;
CREATE TABLE private.debate_category
(
    id   serial NOT NULL,
    PRIMARY KEY (id),
    name text,
    UNIQUE (id)
);
COPY private.debate_category FROM PROGRAM 'sed ''s/\\/\\\\/g'' </home/andrew/hansard/data/debateCategory.tsv';

DROP TABLE IF EXISTS private.debate_column;
CREATE TABLE private.debate_column
(
    id   serial NOT NULL,
    PRIMARY KEY (id),
    name text,
    UNIQUE (id)
);
COPY private.debate_column FROM PROGRAM 'sed ''s/\\/\\\\/g'' </home/andrew/hansard/data/debateColumn.tsv';

DROP TABLE IF EXISTS private.debate_constituency;
CREATE TABLE private.debate_constituency
(
    id   serial NOT NULL,
    PRIMARY KEY (id),
    name text,
    UNIQUE (id)
);
COPY private.debate_constituency FROM PROGRAM 'sed ''s/\\/\\\\/g'' </home/andrew/hansard/data/debateConstituency.tsv';

DROP TABLE IF EXISTS private.debate_entity;
CREATE TABLE private.debate_entity
(
    id   serial NOT NULL,
    PRIMARY KEY (id),
    name text,
    UNIQUE (id)
);
COPY private.debate_entity FROM PROGRAM 'sed ''s/\\/\\\\/g'' </home/andrew/hansard/data/debateEntity.tsv';

DROP TABLE IF EXISTS private.debate_file;
CREATE TABLE private.debate_file
(
    id   serial NOT NULL,
    PRIMARY KEY (id),
    name text,
    UNIQUE (id)
);
COPY private.debate_file FROM PROGRAM 'sed ''s/\\/\\\\/g'' </home/andrew/hansard/data/debateFiles.tsv';

DROP TABLE IF EXISTS private.debate_house;
CREATE TABLE private.debate_house
(
    id   serial NOT NULL,
    PRIMARY KEY (id),
    name text,
    UNIQUE (id)
);
COPY private.debate_house FROM PROGRAM 'sed ''s/\\/\\\\/g'' </home/andrew/hansard/data/debateHouse.tsv';

DROP TABLE IF EXISTS private.debate_image;
CREATE TABLE private.debate_image
(
    id   serial NOT NULL,
    PRIMARY KEY (id),
    name text,
    UNIQUE (id)
);
COPY private.debate_image FROM PROGRAM 'sed ''s/\\/\\\\/g'' </home/andrew/hansard/data/debateImage.tsv';

DROP TABLE IF EXISTS private.debate_label;
CREATE TABLE private.debate_label
(
    id   serial NOT NULL,
    PRIMARY KEY (id),
    name text,
    UNIQUE (id)
);
COPY private.debate_label FROM PROGRAM 'sed ''s/\\/\\\\/g'' </home/andrew/hansard/data/debateLabel.tsv';

DROP TABLE IF EXISTS private.debate_name;
CREATE TABLE private.debate_name
(
    id   serial NOT NULL,
    PRIMARY KEY (id),
    name text,
    UNIQUE (id)
);
COPY private.debate_name FROM PROGRAM 'sed ''s/\\/\\\\/g'' </home/andrew/hansard/data/debateName.tsv';

DROP TABLE IF EXISTS private.debate_speaker;
CREATE TABLE private.debate_speaker
(
    id   serial NOT NULL,
    PRIMARY KEY (id),
    name text,
    UNIQUE (id)
);
COPY private.debate_speaker FROM PROGRAM 'sed ''s/\\/\\\\/g'' </home/andrew/hansard/data/debateSpeaker.tsv';


ALTER TABLE private.debate
    ALTER COLUMN debate_section TYPE integer USING debate_section::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_sentence TYPE integer USING debate_sentence::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_speech TYPE integer USING debate_speech::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_file TYPE integer USING debate_file::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_name TYPE integer USING debate_name::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_category TYPE integer USING debate_category::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_text TYPE integer USING debate_text::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_speaker TYPE integer USING debate_speaker::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_house TYPE integer USING debate_house::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_image TYPE integer USING debate_image::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_column TYPE integer USING debate_column::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_label TYPE integer USING debate_label::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_constituency TYPE integer USING debate_constituency::integer;
ALTER TABLE private.debate
    ALTER COLUMN debate_entity TYPE integer USING debate_entity::integer;

ALTER TABLE private.debate
    ALTER debate_errata TYPE bool USING CASE WHEN debate_errata = 'False' THEN FALSE ELSE TRUE END;
ALTER TABLE private.debate
    ALTER COLUMN debate_errata2 SET DEFAULT FALSE;

/*
ALTER TABLE private.debate ADD CONSTRAINT name_fk FOREIGN KEY (debate_name) REFERENCES  private.debate_name(id);
ALTER TABLE private.debate ADD CONSTRAINT category_fk FOREIGN KEY (debate_category) REFERENCES  private.debate_category(id);
ALTER TABLE private.debate ADD CONSTRAINT text_fk FOREIGN KEY (debate_text) REFERENCES  private.debate_text(id);
ALTER TABLE private.debate ADD CONSTRAINT speaker_fk FOREIGN KEY (debate_speaker) REFERENCES  private.debate_speaker(id);
ALTER TABLE private.debate ADD CONSTRAINT house_fk FOREIGN KEY (debate_house) REFERENCES  private.debate_house(id);
ALTER TABLE private.debate ADD CONSTRAINT image_fk FOREIGN KEY (debate_image) REFERENCES  private.debate_image(id);
ALTER TABLE private.debate ADD CONSTRAINT column_fk FOREIGN KEY (debate_column) REFERENCES  private.debate_column(id);
ALTER TABLE private.debate ADD CONSTRAINT label_fk FOREIGN KEY (debate_label) REFERENCES  private.debate_label(id);
ALTER TABLE private.debate ADD CONSTRAINT constituency_fk FOREIGN KEY (debate_constituency) REFERENCES  private.debate_constituency(id);
*/
CREATE INDEX debate_errata_idx ON private.debate (debate_errata);
CREATE INDEX debate_date_idx ON private.debate (debate_date);
CREATE INDEX debate_section_idx ON private.debate (debate_section);
CREATE INDEX debate_sentence_idx ON private.debate (debate_sentence);
CREATE INDEX debate_house_idx ON private.debate (debate_house);
CREATE INDEX debate_image_idx ON private.debate (debate_image);
CREATE INDEX debate_constituency_idx ON private.debate (debate_constituency);

CREATE INDEX debateText_idx ON private.debate_text USING GIN (to_tsvector('english', fulltext));
