DROP TABLE IF EXISTS private.vocab_classes;
CREATE TABLE private.vocab_classes
(
    id    serial NOT NULL,
    PRIMARY KEY (id),
    class text
        UNIQUE (id)
);
COPY private.vocab_classes FROM '/mnt/volume_nyc3_01/data/classes.csv' WITH (FORMAT csv);

DROP TABLE IF EXISTS private.vocab_offices;
CREATE TABLE private.vocab_offices
(
    id     serial NOT NULL,
    PRIMARY KEY (id),
    office text,
    UNIQUE (id)
);
COPY private.vocab_offices FROM '/mnt/volume_nyc3_01/data/offices.csv' WITH (FORMAT csv);



DROP TABLE IF EXISTS private.vocab_concerns;
CREATE TABLE private.vocab_concerns
(
    id      serial NOT NULL,
    PRIMARY KEY (id),
    concern text,
    UNIQUE (id)
);
COPY private.vocab_concerns FROM '/mnt/volume_nyc3_01/data/concerns.csv' WITH (FORMAT csv);


DROP TABLE IF EXISTS private.vocab_dp;
CREATE TABLE private.vocab_dp
(
    dp text
);
COPY private.vocab_dp FROM '/mnt/volume_nyc3_01/data/dp.csv' WITH (FORMAT csv);
ALTER TABLE private.vocab_dp
    ADD COLUMN id INTEGER;
CREATE SEQUENCE private.vocab_dp_id_seq;
UPDATE private.vocab_dp
SET id = nextval('private.vocab_dp_id_seq');
ALTER TABLE private.vocab_dp
    ALTER COLUMN id SET DEFAULT nextval('private.vocab_dp_id_seq');
ALTER TABLE private.vocab_dp
    ALTER COLUMN id SET NOT NULL;
ALTER TABLE private.vocab_dp
    ADD PRIMARY KEY (id);


DROP TABLE IF EXISTS private.vocab_eminent_domain;
CREATE TABLE private.vocab_eminent_domain
(
    eminent_domain text
);
COPY private.vocab_eminent_domain FROM '/mnt/volume_nyc3_01/data/eminent_domain.csv' WITH (FORMAT csv);
ALTER TABLE private.vocab_eminent_domain
    ADD COLUMN id INTEGER;
CREATE SEQUENCE private.vocab_eminent_domain_id_seq;
UPDATE private.vocab_eminent_domain
SET id = nextval('private.vocab_eminent_domain_id_seq');
ALTER TABLE private.vocab_eminent_domain
    ALTER COLUMN id SET DEFAULT nextval('private.vocab_eminent_domain_id_seq');
ALTER TABLE private.vocab_eminent_domain
    ALTER COLUMN id SET NOT NULL;
ALTER TABLE private.vocab_eminent_domain
    ADD PRIMARY KEY (id);


DROP TABLE IF EXISTS private.vocab_infrastructure;
CREATE TABLE private.vocab_infrastructure
(
    infrastructure text
);
COPY private.vocab_infrastructure FROM '/mnt/volume_nyc3_01/data/infrastructure.csv' WITH (FORMAT csv);
ALTER TABLE private.vocab_infrastructure
    ADD COLUMN id INTEGER;
CREATE SEQUENCE private.vocab_infrastructure_id_seq;
UPDATE private.vocab_infrastructure
SET id = nextval('private.vocab_infrastructure_id_seq');
ALTER TABLE private.vocab_infrastructure
    ALTER COLUMN id SET DEFAULT nextval('private.vocab_infrastructure_id_seq');
ALTER TABLE private.vocab_infrastructure
    ALTER COLUMN id SET NOT NULL;
ALTER TABLE private.vocab_infrastructure
    ADD PRIMARY KEY (id);


DROP TABLE IF EXISTS private.vocab_rights_of_common;
CREATE TABLE private.vocab_rights_of_common
(
    rights_of_common text
);
COPY private.vocab_rights_of_common FROM '/mnt/volume_nyc3_01/data/rights_of_common.csv' WITH (FORMAT csv);
ALTER TABLE private.vocab_rights_of_common
    ADD COLUMN id INTEGER;
CREATE SEQUENCE private.vocab_rights_of_common_id_seq;
UPDATE private.vocab_rights_of_common
SET id = nextval('private.vocab_rights_of_common_id_seq');
ALTER TABLE private.vocab_rights_of_common
    ALTER COLUMN id SET DEFAULT nextval('private.vocab_rights_of_common_id_seq');
ALTER TABLE private.vocab_rights_of_common
    ALTER COLUMN id SET NOT NULL;
ALTER TABLE private.vocab_rights_of_common
    ADD PRIMARY KEY (id);
