/*
Preprocess.js

Slices the Hansard export file, assigning unique IDs
in preperation of Postgres import.

 node --max-old-space-size=29000 ./preprocess.js

Data files are available via M2:
ssh xxx@m2.smu.edu
/scratch/group/pract-txt-mine/data_sets/hansard
rsync -avzr xxx@m2.smu.edu:/scratch/group/pract-txt-mine/data_sets/hansard/hansard_* ~/Desktop/

Assumes headers are as follows, and first line of TSV is headers:

'Constituency',
'Debate',
'debate_id',
'Labels'
'file_section_id',
'Category',
'section_monologue_id',
'section_sentence_id',
'Ententies',
'Errata',
'Speaker',
'House',
'speech_id',
'speechdate',
'Column',
'FileID',
'Image',
'Text',
'sentence_id',
*/
const lineReader = require('line-reader');
const fs = require('fs');

const args = process.argv.slice(2);
console.log(`----------------------------------------------------`);
const inputPath = args[0];
if (!inputPath) {
  console.log('You must provide a full path to the source TSV!');
  console.log(`----------------------------------------------------`);
  process.exit();
} else {
  if (fs.existsSync(inputPath)) {
    console.log(`Processing: ${inputPath}`);
    console.log(`----------------------------------------------------`);
  } else {
    console.log(inputPath);
    console.log('File does not exist!');
    console.log(`----------------------------------------------------`);
    process.exit();
  }
}

let outputDir = './TMP';
if (!fs.existsSync(outputDir)) fs.mkdirSync(outputDir);

const aggregate = (fieldName, field, terminator) => {
  let outputFile = outputFiles[fieldName];
  if (typeof aggregator[fieldName][field] === 'undefined') {
    aggregator[fieldName][field] = aggregatorIndex[fieldName];
    aggregatorIndex[fieldName]++;
    outputFile.write(`${aggregator[fieldName][field]}\t${field.trim()}\n`);
  }
  o_debate.write(`${aggregator[fieldName][field]}${terminator}`);
};

const c_ws = file => {
  return fs.createWriteStream(file);
};
const o_debate = fs.createWriteStream(`${outputDir}/debate.tsv`);
const outputFiles = {};


outputFiles['Constituency'] = c_ws(`${outputDir}/debateConstituency.tsv`);
outputFiles['Debate'] = c_ws(`${outputDir}/debateName.tsv`);
outputFiles['Labels'] = c_ws(`${outputDir}/debateLabel.tsv`);
outputFiles['Category'] = c_ws(`${outputDir}/debateCategory.tsv`);
outputFiles['Ententies'] = c_ws(`${outputDir}/debateEntity.tsv`);
outputFiles['Speaker'] = c_ws(`${outputDir}/debateSpeaker.tsv`);
outputFiles['House'] = c_ws(`${outputDir}/debateHouse.tsv`);
outputFiles['Column'] = c_ws(`${outputDir}/debateColumn.tsv`);
outputFiles['FileID'] = c_ws(`${outputDir}/debateFiles.tsv`);
outputFiles['Image'] = c_ws(`${outputDir}/debateImage.tsv`);
outputFiles['Text'] = c_ws(`${outputDir}/debateText.tsv`);

let hrstart = process.hrtime();
let lastCheck = process.hrtime();

let headers = [];
let aggregator = {};
let aggregatorIndex = {};
let processFields = [
  'Constituency',
  'Debate',
  'Labels',
  'Category',
  'Ententies',
  'Speaker',
  'House',
  'Column',
  'FileID',
  'Image',
  'Text'
];
let importID = 0;
lineReader.eachLine(inputPath, (line, last) => {
  if (importID === 0) {
    headers = line.split('\t');
    headers.map(header => {
      aggregator[header] = {};
      aggregatorIndex[header] = 0;
    });
    console.log(`\n${inputPath}`);
    console.log(headers);
    //o_debate.write(`import_id\t${line}\n`);
  } else {
    o_debate.write(`${importID}\t`);
    let fields = line.split('\t');
    fields.forEach((field, idx) => {
      let terminator =
        headers.length - 1 === headers.indexOf(headers[idx]) ? '' : '\t';
      let fieldName = headers[idx];
      if (
        processFields.includes(fieldName) &&
        (fieldName === 'Text' || fieldName === 'Ententies')
      ) {
        outputFiles[fieldName].write(`${importID}\t${field.trim()}\n`);
        o_debate.write(`${importID}\t`);
      } else if (
        processFields.includes(fieldName) &&
        fieldName === 'Errata'
      ) {
        o_debate.write(`${field.trim().toLowerCase() === 'true' ? 1 : 0}\t`);
      } else {
        if (processFields.includes(fieldName)) {
          aggregate(fieldName, field, terminator);
        } else {
          o_debate.write(`${field.trim()}${terminator}`);
        }
      }
    });
    o_debate.write(`\n`);
    if (importID % 100000 === 0) {
      console.log(`\n${importID} records processed`);
      let minutes = process.hrtime(hrstart)[0] / 60;
      minutes = Math.round(minutes > 0 ? minutes : 0);
      console.log(
        `Elapsed: ${minutes}min | ${
          process.hrtime(hrstart)[0]
        }s, ${process.hrtime(hrstart)[1] / 1000000}ms`
      );
      console.log(`Pace: ${process.hrtime(lastCheck)[0]}s`);
      lastCheck = process.hrtime();
    }
    if (importID % 10000 === 0) process.stdout.write('.');
  }
  if (last) {
    console.log('Finished!');
    process.exit();
  }
  importID++;
});

// Catches all possible exit conditions
const exitHandler = () => {
  o_debate.end();
  Object.keys(outputFiles).forEach(file => {
    outputFiles[file].end();
  });
  process.exit();
};
process.stdin.resume();
process.on('exit', exitHandler);
process.on('SIGINT', exitHandler);
process.on('SIGUSR1', exitHandler);
process.on('SIGUSR2', exitHandler);
process.on('uncaughtException', exitHandler);
