import os
import sys

# Script to analyze data in a flat file.
#
# Expects the first line in the file to be a header row with
# the same delimiter as the rest of the file.
# TODO: auto-detect delimiter

# TODO: make this a command line argument
delimiter = '\t'

# TODO: make this a command line argument
max_unique_size = 50

def main():
    if len(sys.argv) < 2:
        print "This script takes one argument: the input flat data file"
        sys.exit(1)

    path = sys.argv[1]
    print "Path in: " + path
    path_out_stats = path + '.stats.txt'
    print "Path out: " + path_out_stats

    columns = []
    with open(path, 'r') as file_in:
        header_line = file_in.next().strip()
        columns = header_line.split(delimiter)

    num_columns = len(columns)

    # unique_count is a dictionary where the key is the column name
    # and the value is a dictionary where the key is unique values
    # from that column, and the value is a total count of those values.
    unique_count = {}

    # Map of column index -> column name
    # e.g. 3 -> "Phone Number"
    column_index_to_name = {}

    # Map of column name -> column index
    # e.g. "Phone Number" -> 3
    column_name_to_index = {}

    print "Columns:"
    column_index = 0
    for column_name in columns:
        if column_name in unique_count:
            print "Duplicate column name in header: " + column_name
        unique_count[column_name] = {}
        column_index_to_name[column_index] = column_name
        column_name_to_index[column_name] = column_index
        print "Column Name: '" + column_name + "' Column index: " + str(column_index)
        column_index += 1

    with open(path, 'r') as file_in:
        counter = 0
        for line in file_in:
            # Skip the header
            if counter == 0:
                counter += 1
                continue

            line_strip = line.strip('\n').strip('\r')
            line_split = line_strip.split(delimiter)

            if len(line_split) != num_columns:
                print "Wrong number of elements on row: " + str(counter)

            column_index = 0
            for data_value in line_split:
                if len(unique_count[column_index_to_name[column_index]]) >= max_unique_size:
                    continue
                if not data_value in unique_count[column_index_to_name[column_index]]:
                    # Make a new entry for this data value
                    unique_count[column_index_to_name[column_index]][data_value] = 1
                else:
                    # Add to the existing entry for this data_value
                    unique_count[column_index_to_name[column_index]][data_value] += 1
                column_index += 1

            counter += 1

    # Write out the statistics
    with open(path_out_stats, 'w') as stats_file_out:
        # For each column in the input
        for column in columns:
            line_out = "Column Name: " + column + '\n'
            stats_file_out.write(line_out)
            # Write out statistics for that column
            unique_value_counts = unique_count[column]
            unique_values = unique_value_counts.keys()

            line_out = "\tColumn Value: Count\n"
            stats_file_out.write(line_out)
            for unique_value in sorted(unique_value_counts, key=unique_value_counts.get, reverse=True):
                line_out = "\t'" + str(unique_value) + "': " + str(unique_value_counts[unique_value]) + '\n'
                stats_file_out.write(line_out)
            stats_file_out.write('\n')

if __name__ == '__main__':
    main()
