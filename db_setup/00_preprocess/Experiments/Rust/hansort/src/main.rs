#![allow(non_snake_case)]
use std::time::{Duration, Instant};
use chrono::NaiveDate;
use csv::ReaderBuilder;
use std::path::Path;
extern crate serde;
#[macro_use]
extern crate serde_derive;

mod customdate;

#[derive(Debug, Deserialize)]
struct Ententies {
    Ententies: String,
}

#[derive(Debug, Deserialize)]
struct Record {
    ImportID: String,
    FileID: String,
    SectionID: String,
    SentenceID: String,
    Speech_ID: String,
    //#[serde(with = "customdate")]
    //Date: NaiveDate,
    Date: String,
    Debate: String,
    Category: String,
    Text: String,
    Speaker: String,
    Constituency: String,
    House: String,
    Image: String,
    Column: String,
    Errata: String,
    Ententies: String,
    Labels: String
}

fn main() {
    //let input_file = "./hansard_subset.tsv";
    let input_file = "/Users/andrew/Code/Cloud/database.democracylab.io/database/01_import_hansard/TMP/debate.tsv";

    // our eventual output files
    let _outputs = vec![
        "debate.tsv",
        "debateText.tsv",
        "debateFiles.tsv",
        "debateCategory.tsv",
        "debateName.tsv",
        "debateSpeaker.tsv",
        "debateHouse.tsv",
        "debateImage.tsv",
        "debateColumn.tsv",
        "debateLabel.tsv",
        "debateEntity.tsv",
        "debateConstituency.tsv"
    ];
    parse(input_file);
}

fn parse<P: AsRef<Path>>(path: P) {
    let start = Instant::now();

    let mut rdr = ReaderBuilder::new()
        .delimiter(b'\t')
        .from_path(path)
        .unwrap();

    // what we really want is a vec of records, so we can Do Things with it
    let mut results = rdr.deserialize().collect::<Result<Vec<Record>, csv::Error>>().unwrap();
    let mut import_id = 0;
     for record in results.iter() {
         // /println!("{:?} Records: {:?}", results.len(), record.FileID);
         
         if (import_id % 1000 == 0) {
             println!("{}", start.elapsed().as_secs());
             println!("{:?} records processed",import_id);
         }
         import_id = import_id +1;
     }
}
