/*
Assumes headers are as follows, and first line of TSV is headers:

[ 'FileID',
  'SectionID',
  'SentenceID',
  'Speech_ID',
  'Date',
  'Debate',
  'Category',
  'Text',
  'Speaker',
  'Constituency',
  'House',
  'Image',
  'Column',
  'Errata',
  'Ententies',
  'Labels' ]
*/
const lineReader = require('line-reader');
const filePath = '/Volumes/FoxbaseZero/HANSARD/hansard_20190912.tsv';
const fs = require('fs');

const o_debate = fs.createWriteStream('./TMP/00_debate.tsv');
const o_debateText = fs.createWriteStream('./TMP/01_debateText.tsv');
const o_debateFiles = fs.createWriteStream('./TMP/01_debateFiles.tsv');
const o_debateCategory = fs.createWriteStream('./TMP/01_debateCategory.tsv');
const o_debateName = fs.createWriteStream('./TMP/01_debateName.tsv');
const o_debateSpeaker = fs.createWriteStream('./TMP/01_debateSpeaker.tsv');
const o_debateHouse = fs.createWriteStream('./TMP/01_debateHouse.tsv');
const o_debateImage = fs.createWriteStream('./TMP/01_debateImage.tsv');
const o_debateColumn = fs.createWriteStream('./TMP/01_debateColumn.tsv');
const o_debateLabel = fs.createWriteStream('./TMP/01_debateLabel.tsv');
const o_debateEntities = fs.createWriteStream('./TMP/01_debateEntity.tsv');
const o_debateConstituency = fs.createWriteStream(
  './TMP/01_debateConstituency.tsv'
);

var start = new Date();
var hrstart = process.hrtime();

const onComplete = () => {
  o_debate.end();
  o_debateText.end();
  o_debateFiles.end();
  o_debateCategory.end();
  o_debateName.end();
  o_debateSpeaker.end();
  o_debateHouse.end();
  o_debateImage.end();
  o_debateColumn.end();
  o_debateLabel.end();
  o_debateConstituency.end();
  o_debateEntities.end();
  process.exit();
};

const aggregate = (fieldName, field, outputFile) => {
  if (!(fieldName in aggregator)) {
    aggregator[fieldName] = [];
  }
  if (field.length === 0) debugger;
  let fileID_index = aggregator[fieldName].find(el => {
    return el === field;
  });
  if (typeof fileID_index === 'undefined') {
    aggregator[fieldName].push(field);
    outputFile.write(
      `${aggregator[fieldName].indexOf(field)}\t${field.trim()}\n`
    );
  }
  let terminator = fieldName !== 'Labels' ? '\t' : '';
  o_debate.write(`${aggregator[fieldName].indexOf(field)}${terminator}`);
};

let importID = 1;
let headers = [];
let aggregator = {};
lineReader.eachLine(filePath, line => {
  if (line.substring(0, 6) === 'FileID') {
    headers = line.split('\t');
    console.log(`Slicing: ${filePath}`);
    console.log(headers);
  } else {
    o_debate.write(`${importID}\t`);
    let fields = line.split('\t');
    fields.forEach((field, idx) => {
      if (idx === headers.indexOf('Text')) {
        o_debateText.write(`${importID}\t${field.trim()}\n`);
        o_debate.write(`${importID}\t`);
      } else if (idx === headers.indexOf('FileID')) {
        aggregate('FileID', field, o_debateFiles);
      } else if (idx === headers.indexOf('Category')) {
        aggregate('Category', field, o_debateCategory);
      } else if (idx === headers.indexOf('Debate')) {
        aggregate('Debate', field, o_debateName);
      } else if (idx === headers.indexOf('Speaker')) {
        aggregate('Speaker', field, o_debateSpeaker);
      } else if (idx === headers.indexOf('Column')) {
        aggregate('Column', field, o_debateColumn);
      } else if (idx === headers.indexOf('Labels')) {
        aggregate('Labels', field, o_debateLabel);
      } else if (idx === headers.indexOf('Constituency')) {
        aggregate('Constituency', field, o_debateConstituency);
      } else if (idx === headers.indexOf('House')) {
        aggregate('House', field, o_debateHouse);
      } else if (idx === headers.indexOf('Image')) {
        aggregate('Image', field, o_debateImage);
      } else if (idx === headers.indexOf('Ententies')) {
        aggregate('Ententies', field, o_debateEntities);
      } else if (idx === headers.indexOf('Errata')) {
        o_debate.write(`${field.trim().toLowerCase() === 'true' ? 1 : 0}\t`);
      } else {
        o_debate.write(`${field.trim()}\t`);
      }
    });
    o_debate.write(`\n`);
    importID++;
    if (importID % 10000 === 0) {
      console.log(`\n${importID} records processed`);
      var end = new Date() - start,
        hrend = process.hrtime(hrstart);
      console.info('Execution time: %dms', end);
      console.info(
        'Execution time (hr): %ds %dms',
        hrend[0],
        hrend[1] / 1000000
      );
    }
  }
});

process.stdin.resume(); //so the program will not close instantly

const exitHandler = (options, exitCode) => {
  onComplete();
};

//do something when app is closing
process.on('exit', exitHandler.bind(null, { cleanup: true }));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, { exit: true }));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, { exit: true }));
process.on('SIGUSR2', exitHandler.bind(null, { exit: true }));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, { exit: true }));
