UPDATE private.import set debate_text = (SELECT id FROM private.debate_text WHERE private.debate_text.import_id =import.import_id);
UPDATE private.import set debate_file = (SELECT id from private.debate_file WHERE name=import.debate_file);
UPDATE private.import set debate_name = (SELECT id from private.debate_name WHERE name=import.debate_name);
UPDATE private.import set debate_category = (SELECT id from private.debate_category WHERE name=import.debate_category);
UPDATE private.import set debate_speaker = (SELECT id from private.debate_speaker WHERE name=import.debate_speaker);
UPDATE private.import set debate_house = (SELECT id from private.debate_house WHERE name=import.debate_house);
UPDATE private.import set debate_image = (SELECT id from private.debate_image WHERE name=import.debate_image);
UPDATE private.import set debate_column = (SELECT id from private.debate_column WHERE name=import.debate_column);
UPDATE private.import set debate_label = (SELECT id from private.debate_label WHERE name=import.debate_label);
UPDATE private.import set debate_constituency = (SELECT id from private.debate_constituency WHERE name=import.debate_constituency);

ALTER TABLE private.import RENAME TO debate;

ALTER TABLE private.debate RENAME COLUMN debate_file TO file_id;
ALTER TABLE private.debate RENAME COLUMN debate_date TO date;
ALTER TABLE private.debate RENAME COLUMN debate_name TO name_id;
ALTER TABLE private.debate RENAME COLUMN debate_category TO category_id;
ALTER TABLE private.debate RENAME COLUMN debate_text TO text_id;
ALTER TABLE private.debate RENAME COLUMN debate_speaker TO speaker_id;
ALTER TABLE private.debate RENAME COLUMN debate_house TO house_id;
ALTER TABLE private.debate RENAME COLUMN debate_image TO image_id;
ALTER TABLE private.debate RENAME COLUMN debate_column TO column_id;
ALTER TABLE private.debate RENAME COLUMN debate_errata TO errata;
ALTER TABLE private.debate RENAME COLUMN debate_entity TO entity;
ALTER TABLE private.debate RENAME COLUMN debate_label TO label_id;
ALTER TABLE private.debate RENAME COLUMN debate_constituency TO constituency_id;
ALTER TABLE private.debate RENAME COLUMN import_id TO id;

ALTER TABLE private.debate ALTER COLUMN file_id TYPE integer USING file_id::integer;
ALTER TABLE private.debate ALTER COLUMN name_id TYPE integer USING name_id::integer;
ALTER TABLE private.debate ALTER COLUMN category_id TYPE integer USING category_id::integer;
ALTER TABLE private.debate ALTER COLUMN text_id TYPE integer USING text_id::integer;
ALTER TABLE private.debate ALTER COLUMN speaker_id TYPE integer USING speaker_id::integer;
ALTER TABLE private.debate ALTER COLUMN house_id TYPE integer USING house_id::integer;
ALTER TABLE private.debate ALTER COLUMN image_id TYPE integer USING image_id::integer;
ALTER TABLE private.debate ALTER COLUMN column_id TYPE integer USING column_id::integer;
ALTER TABLE private.debate ALTER COLUMN label_id TYPE integer USING label_id::integer;
ALTER TABLE private.debate ALTER COLUMN constituency_id TYPE integer USING constituency_id::integer;

ALTER TABLE private.debate ADD CONSTRAINT name_fk FOREIGN KEY (name_id) REFERENCES  private.debate_name(id);
ALTER TABLE private.debate ADD CONSTRAINT category_fk FOREIGN KEY (category_id) REFERENCES  private.debate_category(id);
ALTER TABLE private.debate ADD CONSTRAINT text_fk FOREIGN KEY (text_id) REFERENCES  private.debate_text(id);
ALTER TABLE private.debate ADD CONSTRAINT speaker_fk FOREIGN KEY (speaker_id) REFERENCES  private.debate_speaker(id);
ALTER TABLE private.debate ADD CONSTRAINT house_fk FOREIGN KEY (house_id) REFERENCES  private.debate_house(id);
ALTER TABLE private.debate ADD CONSTRAINT image_fk FOREIGN KEY (image_id) REFERENCES  private.debate_image(id);
ALTER TABLE private.debate ADD CONSTRAINT column_fk FOREIGN KEY (column_id) REFERENCES  private.debate_column(id);
ALTER TABLE private.debate ADD CONSTRAINT label_fk FOREIGN KEY (label_id) REFERENCES  private.debate_label(id);
ALTER TABLE private.debate ADD CONSTRAINT constituency_fk FOREIGN KEY (constituency_id) REFERENCES  private.debate_constituency(id);
