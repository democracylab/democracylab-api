CREATE UNIQUE INDEX text_pkey1 ON private.debate_text(id int4_ops);
CREATE UNIQUE INDEX IF NOT EXISTS debate_name_pkey  ON private.debate_file(id int4_ops);
CREATE UNIQUE INDEX IF NOT EXISTS debate_category_pkey  ON private.debate_file(id int4_ops);
CREATE UNIQUE INDEX IF NOT EXISTS debate_speaker_pkey  ON private.debate_file(id int4_ops);
CREATE UNIQUE INDEX IF NOT EXISTS debate_house_pkey  ON private.debate_file(id int4_ops);
CREATE UNIQUE INDEX IF NOT EXISTS debate_image_pkey  ON private.debate_file(id int4_ops);
CREATE UNIQUE INDEX IF NOT EXISTS debate_column_pkey  ON private.debate_file(id int4_ops);
CREATE UNIQUE INDEX IF NOT EXISTS debate_label_pkey  ON private.debate_file(id int4_ops);
CREATE UNIQUE INDEX IF NOT EXISTS debate_constituency_pkey  ON private.debate_file(id int4_ops);
