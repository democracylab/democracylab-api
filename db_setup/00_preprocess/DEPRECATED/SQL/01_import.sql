-- NOTE: Do not use these, use the 'script' pipeline instead
-- This will work, but it takes >40 hours to complete

-- IMPORTANT: The order of these columns matter, the import is positional
DROP TABLE IF EXISTS private.import;

CREATE TABLE private.import (
    debate_file text,
    debate_section text,
    debate_sentence text,
    debate_speech text,
    debate_date text,
    debate_name text,
    debate_category text,
    debate_text text,
    debate_speaker text,
    debate_constituency text,
    debate_house text,
    debate_image text,
    debate_column text,
    debate_errata text,
    debate_entity text,
    debate_label text
);

COPY private.import FROM PROGRAM 'sed ''s/\\/\\\\/g'' </mnt/temp/hansard_20190912.tsv';
