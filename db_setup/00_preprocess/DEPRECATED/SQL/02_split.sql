-- After import
-- NOTE: Takes approx 5min
ALTER TABLE private.import ADD COLUMN import_id SERIAL PRIMARY KEY;


-- Structure ----------------------------------------------
CREATE SEQUENCE IF NOT EXISTS private.text_id_seq1;
CREATE TABLE private.debate_text (
    id integer DEFAULT nextval('private.text_id_seq1'::regclass) PRIMARY KEY,
    content text,
    import_id integer
);

CREATE SEQUENCE IF NOT EXISTS private.debate_file_id_seq;
CREATE TABLE private.debate_file (
    id integer DEFAULT nextval('private.debate_file_id_seq'::regclass) PRIMARY KEY,
    name text
);

CREATE SEQUENCE IF NOT EXISTS private.debate_name_id_seq;
CREATE TABLE private.debate_name (
    id integer DEFAULT nextval('private.debate_name_id_seq'::regclass) PRIMARY KEY,
    name text
);

CREATE SEQUENCE IF NOT EXISTS private.debate_category_id_seq;
CREATE TABLE private.debate_category (
    id integer DEFAULT nextval('private.debate_category_id_seq'::regclass) PRIMARY KEY,
    name text
);

CREATE SEQUENCE IF NOT EXISTS private.debate_speaker_id_seq;
CREATE TABLE private.debate_speaker (
    id integer DEFAULT nextval('private.debate_speaker_id_seq'::regclass) PRIMARY KEY,
    name text
);

CREATE SEQUENCE IF NOT EXISTS private.debate_house_id_seq;
CREATE TABLE private.debate_house (
    id integer DEFAULT nextval('private.debate_house_id_seq'::regclass) PRIMARY KEY,
    name text
);

CREATE SEQUENCE IF NOT EXISTS private.debate_image_id_seq;
CREATE TABLE private.debate_image (
    id integer DEFAULT nextval('private.debate_image_id_seq'::regclass) PRIMARY KEY,
    name text
);

CREATE SEQUENCE IF NOT EXISTS private.debate_column_id_seq;
CREATE TABLE private.debate_column (
    id integer DEFAULT nextval('private.debate_column_id_seq'::regclass) PRIMARY KEY,
    name text
);

CREATE SEQUENCE IF NOT EXISTS private.debate_label_id_seq;
CREATE TABLE private.debate_label (
    id integer DEFAULT nextval('private.debate_label_id_seq'::regclass) PRIMARY KEY,
    name text
);

CREATE SEQUENCE IF NOT EXISTS private.debate_constituency_id_seq;
CREATE TABLE private.debate_constituency (
    id integer DEFAULT nextval('private.debate_constituency_id_seq'::regclass) PRIMARY KEY,
    name text
);

-- NOTE: Takes approx 12 min
INSERT INTO private.debate_text (import_id, content) (SELECT import_id, debate_text FROM private.import);
INSERT INTO private.debate_file (name) SELECT DISTINCT (debate_file) FROM private.import;
INSERT INTO private.debate_name (name) SELECT DISTINCT (debate_name) FROM private.import;
INSERT INTO private.debate_category (name) SELECT DISTINCT (debate_category) FROM private.import;
INSERT INTO private.debate_speaker (name) SELECT DISTINCT (debate_speaker) FROM private.import;
INSERT INTO private.debate_house (name) SELECT DISTINCT (debate_house) FROM private.import;
INSERT INTO private.debate_image (name) SELECT DISTINCT (debate_image) FROM private.import;
INSERT INTO private.debate_column (name) SELECT DISTINCT (debate_column) FROM private.import;
INSERT INTO private.debate_label (name) SELECT DISTINCT (debate_label) FROM private.import;
INSERT INTO private.debate_constituency (name) SELECT DISTINCT (debate_constituency) FROM private.import;
