CREATE VIEW API.myendpoint AS (

SELECT q.year, number_of_debates, name FROM (


	SELECT 	count(debate.id) AS number_of_debates,
		left(debate.date, 4) AS year,
		private.debate_speaker.name AS name
	FROM private.debate
		INNER JOIN private.debate_speaker ON debate_speaker.id = debate.speaker_id
	GROUP BY debate.date, debate_speaker.name
	ORDER BY year, number_of_debates DESC


) AS q
INNER JOIN (
	SELECT max(number_of_debates), year FROM (


		SELECT 	count(debate.id) AS number_of_debates,
			left(debate.date, 4) AS year,
			private.debate_speaker.name AS name
		FROM private.debate
			INNER JOIN private.debate_speaker ON debate_speaker.id = debate.speaker_id
		GROUP BY debate.date, debate_speaker.name
		ORDER BY year, number_of_debates DESC


	) AS q2 GROUP BY year
) q3 ON q3.max = q.number_of_debates AND q3.year = q.year
ORDER BY year)


GRANT ALL PRIVILEGES ON  api.myendpoint TO web_anon;
