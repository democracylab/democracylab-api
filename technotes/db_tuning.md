## Quick and Dirty Database Tuning

# Notes
- Database tuning is more art than science, although there are a lot of tools and expertise that can help, there is no "set and forget" or "one size fits all," you will need to experiment. This document runs through some experimentation to prove the point. It is neither an ideal scenario nor does it describe any general purpose heuristics, it's just one "case study"

- Database tuning is a thing-that-I-can-do, but it is not my core competency. People get paid a lot to do **only** this. If you have a mission critical databse (student records, grading system), you should stop reading this and hire a database consultant. Trying to get something running for basic DH work? This document should help you understand one approach.

- If you are considering these issues in light of other projects, you can sidestep all of this trouble by using [completely managed database](https://aws.amazon.com/rds/). There are many providers for this and in most cases it is the best solution. 

>On hosting costs:  One thing that’s interesting, if you do the math all if this is still a lot less than the cost of  owning and operating your own servers, but you can see that the monthly costs reflect the business model this is designed to support - ad driven websites and services. In those cases, you have a direct relationship between number of users/subscribers, server load and monthly cost - one of the challenges I’m trying to sort with regards to hosting DH is that we don’t have this type of user model, it’s not like people pay for accounts or we host ads… so we might increase our server needs without additional “income” to match the pricing. Not sure how to solve this yet, right now I’m trying to see how far I can push the lower-end hardware because I think I can optimize it well enough to handle most academic use cases.


# Observations
- Even before we start, we know a few things:
    - We have one table with 68M rows. Worst case scenario we would need to scan this, but 
    - This is a moslty read-only DB
    - Probably going to get the most boost from RAM, since we're operating on a very large table. The perfect scenario would be a server with as much RAM as the size of the DB. We might guess a machine with 32GB RAM would be awesome, and that's also $180/month.
    - We likely will not have very many concurrent connections/user. Many optimization strategies are focused on this as the bottleneck because the usecase is something like a web service (imagine twitter or facebook, where many many people are reading and writing to the same database many many times a second). In our case we will likely have:
        - Only a few users at a time. Probably less than 20
        - Almost zero write

# Some best practices that we should still do but therefore might not help that much in this case:
- From the postgres docs:` A common strategy is to run VACUUM and ANALYZE once a day during a low-usage time of day.`  True, but on a typical day we would not expect to see ANY deletions or inserts.

# Some decent guides
There are many tools and online guides to best practices around database tuning. Here are a couple that seem fairly useful:
- https://severalnines.com/database-blog/setting-optimal-environment-postgresql
- https://pgtune.leopard.in.ua/#/
- https://stackify.com/postgresql-performance-tutorial/

# PGTune 
For the settings in our test, I'm going to use [PGTune](https://pgtune.leopard.in.ua). We can tune further, but this will get us in the ballpark. One thing that's a bit uncelar is what "type" of databse we should select. Based on what we know about our potential use, and the following guide, I chose 'oltp' (primarily for the DB slightly larger than RAM)
```
https://github.com/gregs1104/pgtune/issues/2#issuecomment-155686952
# dw -- Data Warehouse
#   * Typically I/O- or RAM-bound
#   * Large bulk loads of data
#   * Large complex reporting queries
#   * Also called "Decision Support" or "Business Intelligence"
#
# oltp -- Online Transaction Processing
#   * Typically CPU- or I/O-bound
#   * DB slightly larger than RAM to 1TB
#   * 20-40% small data write queries
#   * Some long transactions and complex read queries
#
# web -- Web Application
#   * Typically CPU-bound
#   * DB much smaller than RAM
#   * 90% or more simple queries
#
# mixed -- Mixed DW and OLTP characteristics
#   * A wide mixture of queries
#
# desktop -- Not a dedicated database
#   * A general workstation, perhaps for a developer
```


sudo service postgresql restart
SHOW config_file;
SHOW ALL;


> Note: As my test query I am running a count() query with DISTINCT, which is a very mean thing to do on a table with millions of rows!
> There is a better way to get this information, namely to pre-compute it, but I'm using this as a  sledgehammer approach to test how various server configurations might impact our overall usage. There's probably a smarter way to do this, but it works :D Ultimately we will use a different approach to get the same information (see below)

1. Run this on our default DB 
`explain analyse select count(distinct debate_image) from private.debate;`

Machine is 1/2/50/2/$10
```
Aggregate  (cost=1390327.09..1390327.10 rows=1 width=8) (actual time=57640.020..57640.021 rows=1 loops=1)
  ->  Index Only Scan using debate_image_idx on debate  (cost=0.57..1221200.33 rows=67650704 width=4) (actual time=6.847..24578.509 rows=68444638 loops=1)
        Heap Fetches: 0
Planning Time: 0.103 ms
Execution Time: 57640.063 ms
```

Increase the machine power to 1/3/50/3/$15
```
Aggregate  (cost=1390327.09..1390327.10 rows=1 width=8) (actual time=36208.299..36208.300 rows=1 loops=1)
  ->  Index Only Scan using debate_image_idx on debate  (cost=0.57..1221200.33 rows=67650704 width=4) (actual time=8.692..18434.178 rows=68444638 loops=1)
        Heap Fetches: 0
Planning Time: 41.739 ms
Execution Time: 36212.661 ms
```
```
ALTER SYSTEM SET max_connections = '20';
ALTER SYSTEM SET shared_buffers = '768MB';
ALTER SYSTEM SET effective_cache_size = '2304MB';
ALTER SYSTEM SET maintenance_work_mem = '192MB';
ALTER SYSTEM SET checkpoint_completion_target = '0.7';
ALTER SYSTEM SET wal_buffers = '16MB';
ALTER SYSTEM SET default_statistics_target = '100';
ALTER SYSTEM SET random_page_cost = '1.1';
ALTER SYSTEM SET effective_io_concurrency = '200';
ALTER SYSTEM SET work_mem = '19660kB';
ALTER SYSTEM SET min_wal_size = '1GB';
ALTER SYSTEM SET max_wal_size = '2GB';
```
No change...

```
ALTER SYSTEM SET max_connections = '20';
ALTER SYSTEM SET shared_buffers = '768MB';
ALTER SYSTEM SET effective_cache_size = '2304MB';
ALTER SYSTEM SET maintenance_work_mem = '192MB';
ALTER SYSTEM SET checkpoint_completion_target = '0.9';
ALTER SYSTEM SET wal_buffers = '16MB';
ALTER SYSTEM SET default_statistics_target = '100';
ALTER SYSTEM SET random_page_cost = '1.1';
ALTER SYSTEM SET effective_io_concurrency = '200';
ALTER SYSTEM SET work_mem = '19660kB';
ALTER SYSTEM SET min_wal_size = '2GB';
ALTER SYSTEM SET max_wal_size = '4GB';
```

```
Aggregate  (cost=1390327.09..1390327.10 rows=1 width=8) (actual time=29688.499..29688.505 rows=1 loops=1)
  ->  Index Only Scan using debate_image_idx on debate  (cost=0.57..1221200.33 rows=67650704 width=4) (actual time=4.922..11832.695 rows=68444638 loops=1)
        Heap Fetches: 0
Planning Time: 17.559 ms
Execution Time: 29688.675 ms
```
Improvement!



- Change server to:  2/2/50/3/15
- 52s (ugh) we get a bigger boost from that extra ram than CPU (as expected)
```
ALTER SYSTEM SET max_connections = '20';
ALTER SYSTEM SET shared_buffers = '512MB';
ALTER SYSTEM SET effective_cache_size = '1536MB';
ALTER SYSTEM SET maintenance_work_mem = '128MB';
ALTER SYSTEM SET checkpoint_completion_target = '0.9';
ALTER SYSTEM SET wal_buffers = '16MB';
ALTER SYSTEM SET default_statistics_target = '100';
ALTER SYSTEM SET random_page_cost = '1.1';
ALTER SYSTEM SET effective_io_concurrency = '200';
ALTER SYSTEM SET work_mem = '26214kB';
ALTER SYSTEM SET min_wal_size = '2GB';
ALTER SYSTEM SET max_wal_size = '4GB';
ALTER SYSTEM SET max_worker_processes = '2';
ALTER SYSTEM SET max_parallel_workers_per_gather = '1';
ALTER SYSTEM SET max_parallel_workers = '2';
 ```
 
 - Change server to:  2/4/50/4/20
Before applying settings below:
```
Aggregate  (cost=1390327.09..1390327.10 rows=1 width=8) (actual time=36115.972..36115.973 rows=1 loops=1)
  ->  Index Only Scan using debate_image_idx on debate  (cost=0.57..1221200.33 rows=67650704 width=4) (actual time=11.910..22755.610 rows=68444638 loops=1)
        Heap Fetches: 0
Planning Time: 68.658 ms
Execution Time: 36121.593 ms
```

After:
```
ALTER SYSTEM SET max_connections = '20';
ALTER SYSTEM SET shared_buffers = '1GB';
ALTER SYSTEM SET effective_cache_size = '3GB';
ALTER SYSTEM SET maintenance_work_mem = '256MB';
ALTER SYSTEM SET checkpoint_completion_target = '0.9';
ALTER SYSTEM SET wal_buffers = '16MB';
ALTER SYSTEM SET default_statistics_target = '100';
ALTER SYSTEM SET random_page_cost = '1.1';
ALTER SYSTEM SET effective_io_concurrency = '200';
ALTER SYSTEM SET work_mem = '52428kB';
ALTER SYSTEM SET min_wal_size = '2GB';
ALTER SYSTEM SET max_wal_size = '4GB';
ALTER SYSTEM SET max_worker_processes = '2';
ALTER SYSTEM SET max_parallel_workers_per_gather = '1';
ALTER SYSTEM SET max_parallel_workers = '2';
``` 

```
Aggregate  (cost=1390327.09..1390327.10 rows=1 width=8) (actual time=23522.560..23522.560 rows=1 loops=1)
  ->  Index Only Scan using debate_image_idx on debate  (cost=0.57..1221200.33 rows=67650704 width=4) (actual time=0.089..9705.707 rows=68444638 loops=1)
        Heap Fetches: 0
Planning Time: 0.821 ms
Execution Time: 23522.732 ms
```



- What happens if we have some serious RAM?
8/32/50/7/$160

20 seconds

```
ALTER SYSTEM SET max_connections = '20';
ALTER SYSTEM SET shared_buffers = '8GB';
ALTER SYSTEM SET effective_cache_size = '24GB';
ALTER SYSTEM SET maintenance_work_mem = '2GB';
ALTER SYSTEM SET checkpoint_completion_target = '0.9';
ALTER SYSTEM SET wal_buffers = '16MB';
ALTER SYSTEM SET default_statistics_target = '100';
ALTER SYSTEM SET random_page_cost = '1.1';
ALTER SYSTEM SET effective_io_concurrency = '200';
ALTER SYSTEM SET work_mem = '104857kB';
ALTER SYSTEM SET min_wal_size = '2GB';
ALTER SYSTEM SET max_wal_size = '4GB';
ALTER SYSTEM SET max_worker_processes = '8';
ALTER SYSTEM SET max_parallel_workers_per_gather = '4';
ALTER SYSTEM SET max_parallel_workers = '8';
```

```
Aggregate  (cost=1390327.09..1390327.10 rows=1 width=8) (actual time=19374.914..19374.914 rows=1 loops=1)
  ->  Index Only Scan using debate_image_idx on debate  (cost=0.57..1221200.33 rows=67650704 width=4) (actual time=0.049..7437.969 rows=68444638 loops=1)
        Heap Fetches: 0
Planning Time: 0.135 ms
Execution Time: 19374.963 ms
```

-- This is a 5 second improvement but costs us $140 additional per month, probably not worth it... let's go back to the $20/mo option and see what else we can optimize here.

Clear the old settings (Which will prevent the server from starting on downgrade)
    - rm /mnt/volume_nyc3_01/postgresql/11/postgresql.auto.conf
    - We're back to exectution time ~26s

## How to do this for real
Count() is always going to be slow, [here's a good explanation](https://www.cybertec-postgresql.com/en/postgresql-count-made-fast/). But now that

Let's try the 'Data Warehouse' settings since we now know our queries will be RAM bound. 

```
ALTER SYSTEM SET max_connections = '20';
ALTER SYSTEM SET shared_buffers = '1GB';
ALTER SYSTEM SET effective_cache_size = '3GB';
ALTER SYSTEM SET maintenance_work_mem = '512MB';
ALTER SYSTEM SET checkpoint_completion_target = '0.9';
ALTER SYSTEM SET wal_buffers = '16MB';
ALTER SYSTEM SET default_statistics_target = '500';
ALTER SYSTEM SET random_page_cost = '1.1';
ALTER SYSTEM SET effective_io_concurrency = '200';
ALTER SYSTEM SET work_mem = '26214kB';
ALTER SYSTEM SET min_wal_size = '4GB';
ALTER SYSTEM SET max_wal_size = '8GB';
ALTER SYSTEM SET max_worker_processes = '2';
ALTER SYSTEM SET max_parallel_workers_per_gather = '1';
ALTER SYSTEM SET max_parallel_workers = '2';
```

-- No real change, just leave it







This query returns us table with the name and the number of times each debate_image appears:

`SELECT debate_image.name, count(distinct debate_image) FROM private.debate 
INNER JOIN private.debate_image ON debate_image.id = debate.debate_image
GROUP BY name
`
It takes 218 seconds to run this query

But we know one important thing:
    - This data will rarely be updated (only when we re-import )
    
We can use a "materialized view," this will take roughly same amount of time to create (218 seconds):
`
create materialized view api.counts as 
SELECT debate_image.name, count(distinct debate_image) FROM private.debate 
INNER JOIN private.debate_image ON debate_image.id = debate.debate_image
GROUP BY name
`

If the underlying data changes (another import) then we will need to refresh this, as follows:
`REFRESH MATERIALIZED VIEW api.counts;`

But we can use it like this (runs in 25 seconds):
`
SELECT * FROM api.counts
`




